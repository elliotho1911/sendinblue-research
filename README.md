## Research on Webhook API

### Webhook events

- Transactional type
  - sent
  - request
  - delivered
  - hardBounce
  - softBounce
  - blocked
  - spam
  - invalid
  - deferred
  - click
  - opened
  - uniqueOpened
  - unsubscribed
- Marketing type webhook

  - spam
  - opened
  - click
  - hardBounce
  - softBounce
  - unsubscribed
  - listAddition
  - delivered

  ### References

- https://developers.sendinblue.com/docs/transactional-webhooks
- https://developers.sendinblue.com/reference/createwebhook

## Observations - when testing with using webhook on dashboard

### Event order when submitting the embedded form

- After the user submitted the form

  - Email sent
  - Email delivered
  - Email opened
  - Email clicked

- After the user clicked on confirmation button

  - Contact added to the list(s) -> "list_addition"
  - Contact's Email subscribed -> "contact_updated" event

### Webhook payload example

```json
{
  "id": 585688,
  "event": "list_addition",
  "email": "example@example.com",
  "list_id": [3],
  "date": "2022-06-14 21:26:23",
  "ts": 1655241983
}
```

### Notes

- no webhook event for campaign
- When user re-open the email -> triggers "contact_updated" event to webhook url
