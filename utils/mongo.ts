import { MongoClient, Db } from "mongodb";
import { MONGODB_URI, DB_NAME } from "../config/api";

let cachedDb: Promise<Db> | undefined = undefined;

export const connectToMongoDB = () => {
  if (cachedDb) {
    return cachedDb;
  }
  cachedDb = MongoClient.connect(MONGODB_URI as string).then((client) =>
    client.db(DB_NAME as string)
  );
  return cachedDb;
};

export const insertEventToDB = async (database: Db, event: any) => {
  const events = database.collection("events");
  const result = await events.insertOne({ event });
  console.log(`Event document was inserted with the _id: ${result.insertedId}`);
};
