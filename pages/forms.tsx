import React from "react";
import { NextPage } from "next";
import EmbeddedForm from "../components/EmbeddedForm";

const forms: NextPage = () => {
  return (
    <div>
      <section>
        <h3 className="text-3xl font-bold text-center">Embedded Form</h3>
        <div>
          <EmbeddedForm />
        </div>
      </section>
    </div>
  );
};

export default forms;
