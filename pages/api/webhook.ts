import { NextApiHandler } from "next";
import { connectToMongoDB, insertEventToDB } from "../../utils/mongo";

const handler: NextApiHandler = async (request, response) => {
  if (request.method === "POST") {
    try {
      const db = await connectToMongoDB();
      await insertEventToDB(db, request.body);
      response.status(200).json(undefined);
    } catch (error) {
      console.error(error);
    }
  }
};

export default handler;
